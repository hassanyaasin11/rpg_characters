# RPG Characters

This is a console application in Java that anyone call `clone` and then play around with. The app has several characters thhat has functionalities the game should provide.

## Install

```
git clone https://gitlab.com/hassanyaasin11/rpg_characters.git
cd rpg_characters
```

## Alternative installation

It is also necessary to download Java Development Kit (JDK), an open source development environment aimed for developing applications on the Java language. The JDK package can be downloaded free on the internet and it contains class libraries, compilers and runtime environment for Java.

## Unit Testing

This application is provided with Unit testing in JUnit 5, there are test methods in this project. The purpose of the tests is to check the functionality of the console application.

## Contributors 

[Hassan Ali Yaasin (@hassanyaasin11)](@hassanyaasin11)

## Licence 

Copyright 2022, Noroff Accelerate AS
