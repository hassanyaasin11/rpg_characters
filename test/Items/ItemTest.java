package Items;

import Characters.Warrior;
import Enums.ArmorTypes;
import Enums.Slot;
import Enums.WeaponTypes;
import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static Characters.Character.*;
import static org.junit.jupiter.api.Assertions.*;

class ItemTest {

    // A private variable is created to be used in this test, warrior will be
    // the character to be used for item testing.
     private Warrior warrior;


    // Warrior will be tested for the Item tests, in this method the Warrior hero will be created to be used
    // in each upcoming test methods in the ItemTest class.
    @BeforeEach
    public void createWarrior() {

        warrior = new Warrior("Warrior", 1, new Armor("Armor", 1, new HashMap<>(), getBaseAttributes()),
                new Weapon("Weapon", new HashMap<>(), 1, 2, 0.5,1), 1);

    }

    // Testing InvalidWeaponException when warrior tries to wear a too high level weapon.
    @Test
    public void highLevelWeaponTest_tooHighLevel_shouldThrowErrorMessage() throws InvalidWeaponException {

        warrior.getWeapon().getWeaponArmed().put(Slot.WEAPON, WeaponTypes.AXES);
        warrior.getWeapon().setMinLevel(2);

        InvalidWeaponException exception = assertThrows(InvalidWeaponException.class, () -> warrior.wearWeapon());
        String errorMessage = exception.getMessage();

        assertTrue(errorMessage.contains("You need to level up!"));
    }

    // Testing InvalidArmorException when warrior tries to wear a too high level armor.
    @Test
    public void highLevelArmorTest_tooHighLevel_shouldThrowErrorMessage() throws InvalidArmorException {

        warrior.getArmor().getEquipment().put(Slot.BODY, ArmorTypes.PLATE);
        warrior.getArmor().setMinLevel(2);

        InvalidArmorException exception = assertThrows(InvalidArmorException.class, () -> warrior.wearArmor());
        String errorMessage = exception.getMessage();

        assertTrue(errorMessage.contains("You need to level up!"));
    }

    // Testing InvalidWeaponException when warrior tries to wear a wrong weapon type.
    @Test
    public void wrongWeaponTypeTest_wrongType_shouldThrowErrorMessage() throws InvalidWeaponException {

        warrior.getWeapon().getWeaponArmed().put(Slot.WEAPON, WeaponTypes.BOWS);

        InvalidWeaponException exception = assertThrows(InvalidWeaponException.class, () -> warrior.wearWeapon());
        String errorMessage = exception.getMessage();

        assertTrue(errorMessage.contains("Invalid!, Warrior cannot wear that weapon"));
    }

    // Testing InvalidArmorException when warrior tries to wear a wrong armor type.
    @Test
    public void wrongArmorTypeTest_wrongType_shouldThrowErrorMessage() throws InvalidArmorException {

        warrior.getArmor().getEquipment().put(Slot.BODY, ArmorTypes.CLOTH);

        InvalidArmorException exception = assertThrows(InvalidArmorException.class, () -> warrior.wearArmor());
        String errorMessage = exception.getMessage();

        assertTrue(errorMessage.contains("Invalid!, Warrior can only have " + ArmorTypes.MAIL + " or " + ArmorTypes.PLATE));
    }

    // Testing if warrior can wear a valid weapon, "AXES" WeaponType will be used in this test as an example.
    @Test
    public void validWeaponTest_validWeapon_shouldBeTrue() throws InvalidWeaponException {

        warrior.getWeapon().getWeaponArmed().put(Slot.WEAPON, WeaponTypes.AXES);

        assertTrue(Weapon.getWeaponArmed().containsValue(WeaponTypes.AXES));
    }

    // Testing if warrior can wear a valid armor, "PLATE" ArmorType will be used in this test as an example.
    @Test
    public void validArmorTest_validArmor_shouldBeTrue() throws InvalidArmorException {

        warrior.getArmor().getEquipment().put(Slot.BODY, ArmorTypes.PLATE);

        assertTrue(Armor.getEquipment().containsValue(ArmorTypes.PLATE));
    }

    // Testing Character damage per second (dps) with no weapon equipped.
    @Test
    public void checkDps_noWeapon_shouldBeCorrectValue() throws InvalidWeaponException {

        double actual = getCharacterDps();

        double expected = 1 * (1 + (5 / 100));
        assertEquals(expected, actual);
    }

    // Testing Character damage per second (dps) with weapon "Axe" equipped.
    @Test
    public void checkDps_weaponAxeEquipped_shouldBeCorrectValue() throws InvalidWeaponException {

        warrior.getWeapon().getWeaponArmed().put(Slot.WEAPON, WeaponTypes.AXES);
        warrior.wearWeapon();

        double result = getCharacterDps();

        double x = 5;
        double y = 100;

        double expected = Math.round((7 * 1.1)*(1 * (1 + x/y)));
        assertEquals(expected, result);
    }

    // Testing Character damage per second (dps) with armor "Plate" equipped.
    @Test
    public void checkDps_armorPlateEquipped_shouldBeCorrectValue() throws InvalidArmorException {

        warrior.getArmor().getEquipment().put(Slot.BODY, ArmorTypes.PLATE);
        warrior.wearArmor();
        double actual = getCharacterDps();

        double x = 5;
        double y = 100;

        double expected = Math.round(1 * (1 * (1 + (x / y))));
        assertEquals(expected, actual);
    }

    // Testing Character damage per second (dps) with weapon "Axe" and armor "Plate" equipped.
    @Test
    public void checkDps_weaponAxeAndArmorPlateEquipped_shouldBeCorrectValue() throws InvalidWeaponException, InvalidArmorException {

        warrior.getWeapon().getWeaponArmed().put(Slot.WEAPON, WeaponTypes.AXES);
        warrior.wearWeapon();

        warrior.getArmor().getEquipment().put(Slot.BODY, ArmorTypes.PLATE);
        warrior.wearArmor();

        double actual = getCharacterDps();

        double x =  5;
        double y = 100;

        double expected = Math.round((7 * 1.1) * (1 + (x+1)/y));
        assertEquals(expected, actual);
    }
}