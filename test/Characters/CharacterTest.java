package Characters;

import Attributes.*;
import Items.*;
import org.junit.jupiter.api.*;

import java.util.HashMap;

import static Characters.Character.*;
import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {

    @Test  //Testing whether created player is level 1, taking Character type Mage as an example.
    public void createPlayer_startLevel_ShouldBeLevel1(){
        Mage mage = new Mage("Warrior", 1, new Armor("Armor", 1, new HashMap<>(), getBaseAttributes()),
                new Weapon("Weapon", new HashMap<>(), 1, 2, 0.5,1), 1);

        int expected = 1;
        int actual = mage.getLevel();

        assertEquals(expected, actual);
    }

    @Test   //Testing whether player gains a level after levelUp() function, with character type Mage as an example.
    public void levelUp_gainLevel_shouldBeLevel2() {
        Mage mage = new Mage("Warrior", 1, new Armor("Armor", 1, new HashMap<>(), getBaseAttributes()),
                new Weapon("Weapon", new HashMap<>(), 1, 2, 0.5,1), 1);

        mage.levelUp();

        int expected = 2;
        int actual = mage.getLevel();

        assertEquals(expected, actual);
    }

    @Test   //Testing Mage Character Level 1 attributes.
    public void createMage_mageStartAttributes_shouldBeLevel1Stats() {
        Mage mage = new Mage("Warrior", 1, new Armor("Armor", 1, new HashMap<>(), getBaseAttributes()),
                new Weapon("Weapon", new HashMap<>(), 1, 2, 0.5,1), 1);

        BasePrimaryAttributes expected = new BasePrimaryAttributes(1,1,8);

        assertEquals(expected.getStrength(), mage.getBaseAttributes().getStrength());
        assertEquals(expected.getDexterity(), mage.getBaseAttributes().getDexterity());
        assertEquals(expected.getIntelligence(), mage.getBaseAttributes().getIntelligence());
    }

    @Test   //Testing Ranger Character Level 1 attributes.
    public void createRanger_mageStartAttributes_shouldBeLevel1Stats() {
        Ranger ranger = new Ranger("Warrior", 1, new Armor("Armor", 1, new HashMap<>(), getBaseAttributes()),
                new Weapon("Weapon", new HashMap<>(), 1, 2, 0.5,1), 1);

        BasePrimaryAttributes expected = new BasePrimaryAttributes(1,7,1);

        assertEquals(expected.getStrength(), ranger.getBaseAttributes().getStrength());
        assertEquals(expected.getDexterity(), ranger.getBaseAttributes().getDexterity());
        assertEquals(expected.getIntelligence(), ranger.getBaseAttributes().getIntelligence());
    }

    @Test   //Testing Rogue Character Level 1 attributes.
    public void createRogue_mageStartAttributes_shouldBeLevel1Stats() {
        Rogue rogue = new Rogue("Warrior", 1, new Armor("Armor", 1, new HashMap<>(), getBaseAttributes()),
                new Weapon("Weapon", new HashMap<>(), 1, 2, 0.5,1), 1);

        BasePrimaryAttributes expected = new BasePrimaryAttributes(2,6,1);

        assertEquals(expected.getStrength(), rogue.getBaseAttributes().getStrength());
        assertEquals(expected.getDexterity(), rogue.getBaseAttributes().getDexterity());
        assertEquals(expected.getIntelligence(), rogue.getBaseAttributes().getIntelligence());
    }

    @Test   //Testing Warrior Character Level 1 attributes.
    public void createWarrior_mageStartAttributes_shouldBeLevel1Stats() {
        Warrior warrior = new Warrior("Warrior", 1, new Armor("Armor", 1, new HashMap<>(), getBaseAttributes()),
                new Weapon("Weapon", new HashMap<>(), 1, 2, 0.5,1), 1);

        BasePrimaryAttributes expected = new BasePrimaryAttributes(5,2,1);

        assertEquals(expected.getStrength(), warrior.getBaseAttributes().getStrength());
        assertEquals(expected.getDexterity(), warrior.getBaseAttributes().getDexterity());
        assertEquals(expected.getIntelligence(), warrior.getBaseAttributes().getIntelligence());
    }

    @Test   //Testing Mage Character attributes after level up.
    public void mageLevelUpAttributes_totalPrimaryAttributes_shouldIncreaseAfterLevelUp() {
        Mage mage = new Mage("Warrior", 1, new Armor("Armor", 1, new HashMap<>(), getBaseAttributes()),
                new Weapon("Weapon", new HashMap<>(), 1, 2, 0.5,1), 1);

        mage.levelUp();

        TotalPrimaryAttributes expected = new TotalPrimaryAttributes(2,2,13);

        assertEquals(expected.getStrength(), mage.getTotalAttributes().getStrength());
        assertEquals(expected.getDexterity(), mage.getTotalAttributes().getDexterity());
        assertEquals(expected.getIntelligence(), mage.getTotalAttributes().getIntelligence());
    }

    @Test   //Testing Ranger Character attributes after level up.
    public void rangerLevelUpAttributes_totalPrimaryAttributes_shouldIncreaseAfterLevelUp() {
        Ranger ranger = new Ranger("Warrior", 1, new Armor("Armor", 1, new HashMap<>(), getBaseAttributes()),
                new Weapon("Weapon", new HashMap<>(), 1, 2, 0.5,1), 1);

        ranger.levelUp();

        TotalPrimaryAttributes expected = new TotalPrimaryAttributes(2,12,2);

        assertEquals(expected.getStrength(), ranger.getTotalAttributes().getStrength());
        assertEquals(expected.getDexterity(), ranger.getTotalAttributes().getDexterity());
        assertEquals(expected.getIntelligence(), ranger.getTotalAttributes().getIntelligence());
    }

    @Test   //Testing Rogue Character attributes after level up.
    public void rogueLevelUpAttributes_totalPrimaryAttributes_shouldIncreaseAfterLevelUp() {
        Rogue rogue = new Rogue("Warrior", 1, new Armor("Armor", 1, new HashMap<>(), getBaseAttributes()),
                new Weapon("Weapon", new HashMap<>(), 1, 2, 0.5,1), 1);

        rogue.levelUp();

        TotalPrimaryAttributes expected = new TotalPrimaryAttributes(3,10,2);

        assertEquals(expected.getStrength(), rogue.getTotalAttributes().getStrength());
        assertEquals(expected.getDexterity(), rogue.getTotalAttributes().getDexterity());
        assertEquals(expected.getIntelligence(), rogue.getTotalAttributes().getIntelligence());
    }

    @Test   //Testing Warrior Character attributes after level up.
    public void warriorLevelUpAttributes_totalPrimaryAttributes_shouldIncreaseAfterLevelUp() {
        Warrior warrior = new Warrior("Warrior", 1, new Armor("Armor", 1, new HashMap<>(), getBaseAttributes()),
                new Weapon("Weapon", new HashMap<>(), 1, 2, 0.5,1), 1);

        warrior.levelUp();

        TotalPrimaryAttributes expected = new TotalPrimaryAttributes(8,4,2);

        assertEquals(expected.getStrength(), warrior.getTotalAttributes().getStrength());
        assertEquals(expected.getDexterity(), warrior.getTotalAttributes().getDexterity());
        assertEquals(expected.getIntelligence(), warrior.getTotalAttributes().getIntelligence());
    }
}