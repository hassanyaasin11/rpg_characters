package Enums;

import Items.Item;

/**
 * These are enums (unchanged constants), of the types of armor the Character could wear depending
 * on the character type.
 */
public enum ArmorTypes {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE
}
