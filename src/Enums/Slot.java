package Enums;

/**
 * These are enums (unchanged constants), of slots (equipment places) where the Character could
 * wear its armor or weapons. Has mapping will be used to store Slot as key mapped together with
 * items (armor and weapons) as values. Weapons can only be placed in "WEAPON" slot while armor can be only be
 * placed in all the other slots.
 *
 */
public enum Slot {
    HEAD,
    BODY,
    LEGS,
    WEAPON
}
