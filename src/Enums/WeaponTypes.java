package Enums;

/**
 * These are enums (unchanged constants), of the types of weapons the Character could wear depending
 * on the character type.
 */
public enum WeaponTypes {
    AXES,
    BOWS,
    DAGGERS,
    HAMMERS,
    STAFFS,
    SWORDS,
    WANDS
}
