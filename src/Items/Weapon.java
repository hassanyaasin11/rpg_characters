package Items;

import Enums.Slot;
import Enums.WeaponTypes;

import java.util.HashMap;

public class Weapon extends Item {

    protected WeaponTypes weaponType;

    protected static double damage=2;
    private static double attackSpeed=0.5;
    //private static double dps = 1;

    //private static double dps = 1;
    private static double dps = damage * attackSpeed;


    /**
     * This is the constructor for the weapon the character could be wearing.
     * @param name
     * @param weaponArmed
     * @param minLevel
     * @param damage
     * @param attackSpeed
     * @param dps
     */
    public Weapon(String name, HashMap<Slot, WeaponTypes> weaponArmed, int minLevel, double damage, double attackSpeed, double dps) {
        super(name, weaponArmed, minLevel);
        this.damage = damage;
        this.attackSpeed = attackSpeed;
        this.dps = dps;
    }

    /**
     * Returns the current weapon type of the weapon, will be taken from enum "WeaponTypes".
     * @return weaponType
     */
    public WeaponTypes getWeaponType() {
        return weaponType;
    }

    /**
     * Sets the new weapon type of the weapon, will be taken from enum "WeaponTypes".
     * @param weaponType
     */
    public void setWeaponType(WeaponTypes weaponType) {
        this.weaponType = weaponType;
    }

    /**
     * Returns the amount of damage the character is able to do.
     * @return damage
     */
    public static double getDamage() {
        return damage;
    }

    /**
     * Sets a new amount of damage the character is able to do.
     * @param damage
     */
    public void setDamage(double damage) {
        this.damage = damage;
    }

    /**
     * Returns the current speed of attack the character is able to perform.
     * @return attackSpeed
     */
    public static double getAttackSpeed() {
        return attackSpeed;
    }

    /**
     * Sets a new speed of attack the character is able to perform.
     * @param attackSpeed
     */
    public void setAttackSpeed(double attackSpeed) {
        this.attackSpeed = attackSpeed;
    }

    /**
     * Damage per second (dps) is calculated by dps=damage*attackSpeed
     * @return dps
     */
    public static double getDps() {
        return dps;
    }

    /**
     * Damage per second (dps) is calculated by dps=damage*attackSpeed
     * @param dps
     */
    public void setDps(double dps) {
        this.dps = dps;
    }

    /**
     * The info of the weapon the character is wearing will be displayed here.
     * @return info of the weapon.
     */
    @Override
    public String toString() {
        return "Weapons{" +
                ", name='" + name + '\'' +
                ", level=" + minLevel +
                ", armed weapon=" + weaponArmed +
                "weaponType=" + weaponType +
                ", damage=" + damage +
                ", attackSpeed=" + attackSpeed +
                ", dps=" + dps +
                '}';
    }
}
