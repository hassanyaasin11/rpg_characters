package Items;

import Attributes.BasePrimaryAttributes;
import Enums.ArmorTypes;
import Enums.Slot;

import java.util.HashMap;

public class Armor extends Item {

    private ArmorTypes armorType;

    //protected HashMap<Slot, ArmorTypes> equipment;
    protected BasePrimaryAttributes basePrimaryAttributes;

    /**
     * This is the constructor for the armor the character could be wearing.
     * @param name
     * @param minLevel
     * @param equipment
     * @param basePrimaryAttributes
     */
    public Armor(String name, int minLevel, HashMap<Slot, ArmorTypes> equipment, BasePrimaryAttributes basePrimaryAttributes) {
        super(name, minLevel, equipment);
        this.basePrimaryAttributes = basePrimaryAttributes;
    }

    /**
     * Returns the current armor type of the armor, will be taken from enum "ArmorTypes".
     * @return armorType
     */
    public ArmorTypes getArmorType() {
        return armorType;
    }

    /**
     * Sets the new armor type of the armor, will be taken from enum "ArmorTypes".
     * @param armorType
     */
    public void setArmorType(ArmorTypes armorType) {
        this.armorType = armorType;
    }

    /**
     * Returns the current basePrimaryAttributes the armor will add to the character. This is an extra feature
     * that will be used in this project whether there is time.
     * @return basePrimaryAttributes
     */
    public BasePrimaryAttributes getBasePrimaryAttributes() {
        return basePrimaryAttributes;
    }

    /**
     * Sets the new basePrimaryAttributes the armor will add to the character. This is an extra feature
     * that will be used in this project whether there is time.
     *  @param basePrimaryAttributes
     */
    public void setBasePrimaryAttributes(BasePrimaryAttributes basePrimaryAttributes) {
        this.basePrimaryAttributes = basePrimaryAttributes;
    }

    /**
     * The info of the armor the character is wearing will be displayed here.
     * @return info of the armor.
     */
    @Override
    public String toString() {
        return "Armor{" +
                ", name=" + name +
                ", armorType=" + armorType +
                ", level=" + minLevel +
                ", equipment=" + equipment +
                ", basePrimaryAttributes=" + basePrimaryAttributes +
                '}';
    }
}
