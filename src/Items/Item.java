package Items;

import Enums.ArmorTypes;
import Enums.Slot;
import Enums.WeaponTypes;

import java.util.HashMap;

public abstract class Item {

    // Fields - State
    protected String name;
    protected static int minLevel;

    protected static HashMap<Slot, ArmorTypes> equipment;
    protected static HashMap<Slot, WeaponTypes> weaponArmed;

    // Constructors

    /**
     * The Item class will be an abstract parent class for both Armor class and Weapon class.
     * This is why there are two constructors for Item, first one is for Armor class and second one
     * is for Weapon class. Each Item (Armor and Weapon) will have name, required level to equip the item
     * and Slot where the item will be equipped. HashMapping was used to pair each slot as key to
     * item (both armor and weapon) as values.
     * to slot
     */

    /**
     * This is the constructor for the Armor class with the properties; name of the armor, required level
     * to wear the armor and a HashMap variable "equipment" where "Slot" is mapped to "ArmorTypes".
     * @param name
     * @param minLevel
     * @param equipment
     */
    public Item(String name, int minLevel, HashMap<Slot, ArmorTypes> equipment) {
        this.name = name;
        this.minLevel = minLevel;
        this.equipment = equipment;
    }

    /**
     * This is the constructor for the Weapon class with the properties; name of the wepon, required level
     * to wear the weapon and a HashMap variable "weaponArmed" where "Slot" is mapped to "WeaponTypes".
     * @param name
     * @param weaponArmed
     * @param minLevel
     */
    public Item(String name, HashMap<Slot, WeaponTypes> weaponArmed,  int minLevel) {
        this.name = name;
        this.weaponArmed = weaponArmed;
        this.minLevel = minLevel;
    }

    //Methods
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static int getMinLevel() {
        return minLevel;
    }

    public void setMinLevel(int minLevel) {
        this.minLevel = minLevel;
    }

    public static HashMap<Slot, ArmorTypes> getEquipment() {
        return equipment;
    }

    public void setEquipment(HashMap<Slot, ArmorTypes> equipment) {
        this.equipment = equipment;
    }

    public static HashMap<Slot, WeaponTypes> getWeaponArmed() {
        return weaponArmed;
    }

    public void setWeaponArmed(HashMap<Slot, WeaponTypes> weaponArmed) {
        this.weaponArmed = weaponArmed;
    }
}
