package Attributes;

public class TotalPrimaryAttributes {

    // Fields -States
    private double strength = 0;
    private double dexterity = 0;
    private double intelligence = 0;

    // Constructor

    /**
     * This constructor is for the total primary attributes, the attributes includes the strength,
     * dexterity and strength the character has together with armor or weapon.
     * Total primary attributes should increase depending on the armor or weapon
     * the character is wearing. Total primary attributes is calculated by this formula; Total attribute = base + attributes from all equipped armor.
     * @param strength
     * @param dexterity
     * @param intelligence
     */
    public TotalPrimaryAttributes(double strength, double dexterity, double intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    // Methods

    /**
     * returns current base strength.
     * @return strength
     */
    public double getStrength() {
        return strength;
    }

    /**
     * sets new strength base attribute.
     * @param strength
     */
    public void setStrength(double strength) {
        this.strength = strength;
    }

    /**
     * returns current dexterity base attribute.
     * @return dexterity
     */
    public double getDexterity() {
        return dexterity;
    }

    /**
     * sets new dexterity base attribute.
     * @param dexterity
     */
    public void setDexterity(double dexterity) {
        this.dexterity = dexterity;
    }

    /**
     * returns current intelligence base attribute.
     * @return intelligence
     */
    public double getIntelligence() {
        return intelligence;
    }

    /**
     * sets new intelligence attribute.
     * @param intelligence
     */
    public void setIntelligence(double intelligence) {
        this.intelligence = intelligence;
    }


    /**
     * To show the total primary attributes in string form.
     * @return totalPrimaryAttributes
     */
    @Override
    public String toString() {
        return "totalPrimaryAttributes{" +
                "strength=" + strength +
                ", dexterity=" + dexterity +
                ", intelligence=" + intelligence +
                '}';
    }
}
