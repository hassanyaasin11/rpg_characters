package Characters;
import Attributes.*;
import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import Items.Armor;
import Items.Weapon;


public abstract class  Character {

    // Fields - state
    protected static String name;
    protected static int level = 1;
   // protected static CharacterTypes characterType;
    protected static BasePrimaryAttributes baseAttributes;
    protected static TotalPrimaryAttributes totalAttributes;

    protected static Armor armor;
    protected static Weapon weapon;

    protected static double characterDps=1;

    // Constructor

    // Default / empty constructor
    public Character() {
    }

    // Custom constructor

    /**
     * Creates Character with important properties such as name the user could choose, level the character
     * has during the game, the base primary attributes, the total primary attributes, the armor and the weapon
     * the character is wearing, and the Character Damage Per Second (characterDps). Character DPS is how much
     * damage the Character can deal per second, it is calculated by this formula; Character DPS = Weapon DPS * (1 + TotalMainPrimaryAttribute/100).
     * the
     * @param name
     * @param level
     * @param baseAttributes
     * @param totalAttributes
     * @param armor
     * @param weapons
     * @param characterDps
     */
    public Character(String name, int level, BasePrimaryAttributes baseAttributes, TotalPrimaryAttributes totalAttributes, Armor armor, Weapon weapons, double characterDps) {
        this.name = name;
        this.level = level;
        //this.characterType = characterType;
        this.baseAttributes = baseAttributes;
        this.totalAttributes = totalAttributes;
        this.armor = armor;
        this.weapon = weapons;
        this.characterDps = characterDps;
    }


    // Methods

    /**
     * Returns name of Character.
     * @return name
     */
    public static String getName() {
        return name;
    }

    /**
     * Sets name of Character.
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns current level of Character.
     * @return level
     */
    public static int getLevel() {
        return level;
    }

    /**
     * Sets new level of Character.
     * @param level
     */
    public void setLevel(int level) {
        this.level = level;
    }

    /**
     * Returns the current base primary attributes of the Character.
     * @return baseAttributes
     */
    public static BasePrimaryAttributes getBaseAttributes() {
        return baseAttributes;
    }

    /**
     * Sets the new base primary attributes of the Chracter.
     * @param baseAttributes
     */
    public void setBaseAttributes(BasePrimaryAttributes baseAttributes) {
        this.baseAttributes = baseAttributes;
    }

    /**
     * Returns the current total primary attributes of the Character.
     * @return totalAttributes
     */
    public TotalPrimaryAttributes getTotalAttributes() {
        return totalAttributes;
    }

    /**
     * Sets the new total primary attributes of the Chracter.
     * @param totalAttributes
     */
    public void setTotalAttributes(TotalPrimaryAttributes totalAttributes) {
        this.totalAttributes = totalAttributes;
    }

    /**
     * Returns the current armor the Character is wearing.
     * @return armor
     */
    public static Armor getArmor() {
        return armor;
    }

    /**
     * Sets the new armor the Character will wear.
     * @param armor
     */
    public void setArmor(Armor armor) {
        this.armor = armor;
    }

    /**
     * Returns the current weapons the Character is wearing.
     * @return weapons
     */
    public static Weapon getWeapon() {
        return weapon;
    }

    /**
     * Sets the new weapons the Character will wear.
     * @param weapon
     */
    public static void setWeapon(Weapon weapon) {
        Character.weapon = weapon;
    }

    /**
     * Returns dps (damage per second) of weapon which is Character dps = Weapon DPS * (1 + TotalMainPrimaryAttribute/100)
     * at the beginning since the Character is not wearing any weapons,
     * Character dps should be dps= 1 * (1 + TotalMainPrimaryAttribute/100) since Weapon DPS is 1. After equipping weapons should the
     * Character dps (damage per second) change and adapt value through this formula; Character DPS = Weapon DPS * (1 + TotalMainPrimaryAttribute/100)
     * @return weapons.getDps()
     */
    public static double getCharacterDps() {

            return characterDps;
    }


    /**
     * Sets the new Character dps (damage per second) by using this
     * formula; Character DPS = Weapon DPS * (1 + TotalMainPrimaryAttribute/100).
     * @param characterDps
     */
    public static void setCharacterDps(double characterDps) {
        Character.characterDps = characterDps;
    }

    /**
     * This abstract method is the levelUp() function, each Character type will level up through this function.
     * When leveling up, the level of the Character should increment while the attributes (base and total)
     * should increase depending on the Character type.
     */
    public abstract void levelUp();

    /**
     * This abstract method is the addArmor() function, each Character type will wear armor though this function.
     * When wearing armor, the attributes (only base attributes) of the Character should increase depending on the Character type.
     * Some Character types can only wear some type of armor, when a character type tries to wear something it shouldn't,
     * this function should throw an exception message which will be the "InvalidArmorException" created for this method.
     * @throws InvalidArmorException
     */
    public abstract void wearArmor() throws InvalidArmorException;

    /**
     * This abstract method is the addWeapon() function, each Character type will wear weapon through this function.
     * When wearing weapons, the damage, attack speed and weapon dps (damage per second) should increase together
     * through this formula; dps=damage*attack speed.
     * Some Character types can only wear some type of weapon, when a character type tries to wear something it shouldn't,
     * this function should throw an exception message which will be the "InvalidWeaponException" created for this method.
     * @throws InvalidWeaponException
     */
    public abstract void wearWeapon() throws InvalidWeaponException;

    /**
     * This is the Character stats display of the game.
     * This should show the properties of the Character object,
     * This method uses simple strings from different classes generated into a StringBuilder
     * showing the stats of the character.
     * @return properties of Character
     */
    public static void characterStatsDisplay() {
        StringBuilder statsDisplay = new StringBuilder();
        statsDisplay.append("name=" + name +
                ", level=" + level +
                ", baseAttributes=" + baseAttributes +
                ", totalAttributes=" + totalAttributes +
                ", armor=" + armor +
                ", weapons=" + weapon +
                ", characterDps=" + characterDps );

        System.out.println(statsDisplay);
    }
}
