package Characters;

import Attributes.BasePrimaryAttributes;
import Attributes.TotalPrimaryAttributes;
import Enums.ArmorTypes;
import Enums.Slot;
import Enums.WeaponTypes;
import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import Items.Armor;
import Items.Item;
import Items.Weapon;

public class Ranger extends Character {

    public Ranger() {
    }

    /**
     * This is the Ranger Character, the functions that are abstracted will be more coded in this class according to functionalities
     * of the Ranger Character.
     * @param name
     * @param level
     * @param armor
     * @param weapons
     * @param characterDps
     */
    public Ranger(String name, int level, Armor armor, Weapon weapons, double characterDps) {
        super(name, 1, new BasePrimaryAttributes(1,7,1),
                new TotalPrimaryAttributes(1,7,1), armor, weapons, characterDps);
    }

    /**
     * The levelUp() function should increment the "level" and increase the attributes
     * according to the Warrior character type, the damage, attack speed and damage per second (dps)
     * should also be increased when the character is leveling up.
     */
    @Override
    public void levelUp() {
        // Increment level of the character
        setLevel(getLevel()+1);

        // Increases the base attributes.
        baseAttributes.setStrength(baseAttributes.getStrength()+1);
        baseAttributes.setDexterity(baseAttributes.getDexterity()+5);
        baseAttributes.setIntelligence(baseAttributes.getIntelligence()+1);

        // In case the character already has no armor or weapon, update the total attributes after the increased base attributes.
        if(armor.getEquipment().containsValue(null) && weapon.getWeaponArmed().containsValue(null)) {
            totalAttributes.setStrength(baseAttributes.getStrength());
            totalAttributes.setDexterity(baseAttributes.getDexterity());
            totalAttributes.setIntelligence(baseAttributes.getIntelligence());
        }

        // In case the character already has armor or weapon, to increase total attributes together with the items.
        else {
            totalAttributes.setStrength(totalAttributes.getStrength()+1);
            totalAttributes.setDexterity(totalAttributes.getDexterity()+5);
            totalAttributes.setIntelligence(totalAttributes.getIntelligence()+1);
        }

        // Increase the damage, attack speed and damage per second (dps) of the character.
        weapon.setDamage(weapon.getDamage()+(0.01*totalAttributes.getDexterity()));
        weapon.setAttackSpeed(weapon.getAttackSpeed()+2);
        weapon.setDps((weapon.getDamage()* weapon.getAttackSpeed()));

        //Character DPS = Weapon DPS * (1 + TotalMainPrimaryAttribute/100), getCharacterDps() function returns the weapon dps;
        setCharacterDps(getCharacterDps()*(1+totalAttributes.getDexterity()/100));
    }

    /**
     * This addArmor() function will give the Character to wear armor according to the rules of the game.
     * This function should throw "InvalidArmorException" when the character tries to go against the rules in the game.
     * @throws InvalidArmorException
     */
    @Override
    public void wearArmor() throws InvalidArmorException {

        // Ranger can only wear Mail or Leather
        if((armor.getEquipment().containsValue(ArmorTypes.MAIL) || armor.getEquipment().containsValue(ArmorTypes.LEATHER)) &&
                !armor.getEquipment().containsValue(ArmorTypes.CLOTH) &&
                !armor.getEquipment().containsValue(ArmorTypes.PLATE)){

                // Armor will increase the attributes
                totalAttributes.setStrength((baseAttributes.getStrength()) + 1);
                totalAttributes.setDexterity((baseAttributes.getDexterity()) + 7);
                totalAttributes.setIntelligence((baseAttributes.getIntelligence()) + 1);

        }
        else
            throw new InvalidArmorException("Invalid!, Ranger can only have " + ArmorTypes.MAIL + "or" + ArmorTypes.LEATHER);

        // Weapon cannot be armed in "WEAPON" Slot.
        if(!armor.getEquipment().containsKey(Slot.WEAPON)) {

            if(armor.getEquipment().size()==0) {
                // Armor will increase the attributes
                totalAttributes.setStrength((baseAttributes.getStrength()) + 1);
                totalAttributes.setDexterity((baseAttributes.getDexterity()) + 7);
                totalAttributes.setIntelligence((baseAttributes.getIntelligence()) + 1);

            }
        }
        else
            throw new InvalidArmorException("You cannot equip armor in slot " + Slot.WEAPON);

        // Some Item have required level for the character to wear them.
        if(getLevel() >= Item.getMinLevel()) {

            if(armor.getEquipment().size()==0) {
                // Armor will increase the attributes
                totalAttributes.setStrength((baseAttributes.getStrength()) + 1);
                totalAttributes.setDexterity((baseAttributes.getDexterity()) + 7);
                totalAttributes.setIntelligence((baseAttributes.getIntelligence()) + 1);

            }
        }
        else
            throw new InvalidArmorException("You need to level up!");
    }

    /**
     * This addWeapon() function will give the Character to wear weapon according to the rules of the game.
     * This function should throw "InvalidWeaponException" when the character tries to go against the rules in the game.
     * @throws InvalidWeaponException
     */
    @Override
    public void wearWeapon() throws InvalidWeaponException {

        //CharacterDps should be CharacterDPS= 1 * (1 + TotalMainPrimaryAttribute / 100)) in case no weapon is equipped.
        setCharacterDps(Math.round(1*(1+(totalAttributes.getStrength()/100))));

        // Ranger can only have Bows.
        if(weapon.getWeaponArmed().containsValue(WeaponTypes.BOWS) &&
                !weapon.getWeaponArmed().containsValue(WeaponTypes.STAFFS) &&
                !weapon.getWeaponArmed().containsValue(WeaponTypes.WANDS) &&
                !weapon.getWeaponArmed().containsValue(WeaponTypes.DAGGERS) &&
                !weapon.getWeaponArmed().containsValue(WeaponTypes.SWORDS) &&
                !weapon.getWeaponArmed().containsValue(WeaponTypes.AXES) &&
                !weapon.getWeaponArmed().containsValue(WeaponTypes.HAMMERS)) {

                // Weapon will increase the damage and the attack speed the character could deal.
                weapon.setDamage(weapon.getDamage() + 1);
                weapon.setAttackSpeed(weapon.getAttackSpeed() + 2);

                // Damage per second (dps) will also be increased since dps = damage * attack speed.
                weapon.setDps((weapon.getDamage() * weapon.getAttackSpeed()));

                // Weapon will increase Character damage per second (dps) through
                // this calculation, Character DPS = Weapon DPS * (1 + TotalMainPrimaryAttribute/100).
                setCharacterDps(getCharacterDps() * (1 + totalAttributes.getDexterity() / 100));
        }
        else
            throw new InvalidWeaponException("Invalid!, Ranger cannot wear that weapon");

        // Weapons cannot be pu in other Slots than "WEAPON" Slot.
        if(!weapon.getWeaponArmed().containsKey(Slot.HEAD) &&
                !weapon.getWeaponArmed().containsKey(Slot.BODY) &&
                !weapon.getWeaponArmed().containsKey(Slot.LEGS))
        {

            if(weapon.getWeaponArmed().size()==0) {
                // Weapon will increase the damage and the attack speed the character could deal.
                weapon.setDamage(weapon.getDamage() + 1);
                weapon.setAttackSpeed(weapon.getAttackSpeed() + 2);

                // Damage per second (dps) will also be increased since dps = damage * attack speed.
                weapon.setDps((weapon.getDamage() * weapon.getAttackSpeed()));

                // Weapon will increase Character damage per second (dps) through
                // this calculation, Character DPS = Weapon DPS * (1 + TotalMainPrimaryAttribute/100).
                setCharacterDps(getCharacterDps() * (1 + totalAttributes.getDexterity() / 100));
            }
        }
        else
            throw new InvalidWeaponException("You can only equip weapons in WEAPONS slot");

        // Some Item have required level for the character to wear them.
        if(getLevel() >= Item.getMinLevel()) {

            if(Item.getWeaponArmed().size()==0) {
                // Weapon will increase the damage and the attack speed the character could deal.
                weapon.setDamage(weapon.getDamage() + 1);
                weapon.setAttackSpeed(weapon.getAttackSpeed() + 2);

                // Damage per second (dps) will also be increased since dps = damage * attack speed.
                weapon.setDps((weapon.getDamage() * weapon.getAttackSpeed()));

                // Weapon will increase Character damage per second (dps) through
                // this calculation, Character DPS = Weapon DPS * (1 + TotalMainPrimaryAttribute/100).
                setCharacterDps(getCharacterDps() * (1 + totalAttributes.getDexterity() / 100));
            }
        }
        else
            throw new InvalidWeaponException("You need to level up!");


    }


}
