package Exceptions;

/**
 * This is the InvalidWeaponException class. The "InvalidWeaponException" will be called when
 * a certain type of character tries to arm a weapon that does not belong to its character type.
 */
public class InvalidWeaponException extends Exception {

    public String message;

    /**
     *
     * @param message
     */
    public InvalidWeaponException(String message) {
        this.message = message;
    }

    /**
     * This should return the error message, will be used in the item testing for weapon.
     * @return message
     */
    @Override
    public String getMessage() {
        return message;
    }

    /**
     *
     * @param message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     *
     * @return error message
     */
    @Override
    public String toString() {
        return "InvalidWeaponException{" +
                "message='" + message + '\'' +
                '}';
    }
}