package Exceptions;

/**
 * This is the InvalidArmorException class. The "InvalidArmorException" will be called when
 * a certain type of character tries to equip an armor that does not belong to its character type.
 */
public class InvalidArmorException extends Throwable {

    public String message;

    /**
     *
     * @param message
     */
    public InvalidArmorException(String message) {
        this.message = message;
    }

    /**
     * This should return the error message, will be used in the item testing for armor.
     * @return message
     */
    @Override
    public String getMessage() {
        return message;
    }

    /**
     *
     * @param message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     *
     * @return error message
     */
    @Override
    public String toString() {
        return "InvalidArmorException{" +
                "message='" + message + '\'' +
                '}';
    }
}
